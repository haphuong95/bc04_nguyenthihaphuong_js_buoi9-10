function taoDiv() {
  var contentHTML = "";
  for (var i = 1; i <= 10; i++) {
    // biến đếm là i
    if (i % 2 == 1) {
      contentHTML += `<p class=" alert bg-primary text-white">Div lẻ ${i}</p>`;
    } else {
      contentHTML += `<p class=" alert bg-danger text-white">Div chẵn ${i}</p>`;
    }
  }
  document.getElementById("result").innerHTML = contentHTML;
}
