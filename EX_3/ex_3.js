function tinh() {
  var soN = document.getElementById("so-n").value * 1;
  var giaiThua = 1;
  if (soN <= 0) {
    giaiThua = 1;
  } else {
    for (var i = 1; i <= soN; i++) {
      // biến đếm là i
      giaiThua = giaiThua * i;
    }
  }
  document.getElementById(
    "result"
  ).innerHTML = `<h3 class="py-4"> Tổng là: ${giaiThua.toLocaleString()}</h3>`;
}
